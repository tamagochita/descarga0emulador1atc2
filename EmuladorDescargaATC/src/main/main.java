/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import consume.ParserJSON;
import fixedDate.FixedData;
import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;
import org.jpos.core.Sequencer;
import org.jpos.core.VolatileSequencer;

/**
 *
 * @author gochi
 */
public class main {

    public static Sequencer seq = new VolatileSequencer();
    public static ParserJSON rspJson = new ParserJSON();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        loadFileProperties();

    }

    private static void loadFileProperties() {

        Properties propObj = new Properties();
        File propFile = new File(FixedData.CONFIG_FILE);

        if (!propFile.exists()) {
            //Create Log4j Logger
            org.apache.log4j.PropertyConfigurator.configure(FixedData.CONFIG_FILE);
            System.out.println("Archivo de Propiedades No Encontrado, Nombre: " + FixedData.CONFIG_FILE + " - Emulador " + FixedData.VERSION);
            System.exit(-1);
            return;   ///Init Error No Properties File
        }

        try {
            propObj.load(new FileInputStream(FixedData.CONFIG_FILE));
        } catch (Exception e) {
            //Create Log4j Logger
            org.apache.log4j.PropertyConfigurator.configure(FixedData.CONFIG_FILE);
            System.out.println("Error Leyendo Archivo de Propiedades - Emulador " + FixedData.VERSION);
            System.exit(-1);
            return;   ///Error Reading Properties File
        }

        //Check Init Data via Properties File - Server Keys
        if (propObj.getProperty(FixedData.TERMINAL_ID) == null
                || propObj.getProperty(FixedData.TPDU) == null) {
            System.out.println("Algunas Propiedades del Servidor No Encontradas, Revisar Archivo de Propiedades - Emulator " + FixedData.VERSION);
            System.exit(-1);
            return;   ///Init Data Empty
        } else {
            //Init Server Properties

        }

    }

}
