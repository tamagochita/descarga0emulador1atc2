/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package consume;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 *
 * @author WPOSS
 */
public class ParserJSON {
      
    private String rsp;
    private String xml;
    private String archivo;
    private String paquete;
    private String version; 
    
    public void parse(String jsonLine) {       
        //jsonString is of type java.lang.String
        JsonObject jsonObject = JsonParser.parseString​(jsonLine).getAsJsonObject();
        
        rsp = jsonObject.get("res").getAsString();        
        xml = jsonObject.get("xml").getAsString();        
        archivo = jsonObject.get("archivo").getAsString();        
        paquete = jsonObject.get("paquete").getAsString();        
        version = jsonObject.get("version").getAsString();        
       
    }

    public String getRsp() {
        return rsp;
    }

    public String getXml() {
        return xml;
    }

    public String getArchivo() {
        return archivo;
    }

    public String getPaquete() {
        return paquete;
    }

    public String getVersion() {
        return version;
    }
}
