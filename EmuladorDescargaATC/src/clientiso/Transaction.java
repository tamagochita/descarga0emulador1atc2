/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clientiso;

import java.io.IOException;
import org.jpos.iso.BaseChannel;
import org.jpos.iso.ISOChannel;
import org.jpos.iso.ISOException;
import org.jpos.iso.channel.NACChannel;
import org.jpos.util.LogSource;
import org.jpos.util.Logger;
import org.jpos.util.SimpleLogListener;

/**
 *
 * @author WPOSS
 */
public class Transaction {

    private final int id;

    public Transaction(int idx) {
        this.id = idx;
    }

    public void run() {

        try {
            ///Crear Logger
            Logger logger = new Logger();
            logger.addListener(new SimpleLogListener(System.out));

            ///Crear Channel
            byte[] TPDU = {0x60, 0x01, 0x11, 0x00, 0x00};
            ///Tipo de Canal y de Empaquetador
            ISOChannel channel = new NACChannel("10.122.244.142", 2304, new ISO87BPackager_TeleLoader(), TPDU);

            ((LogSource) channel).setLogger(logger, "ClientChannel");
            ((BaseChannel) channel).setTimeout(60000);   ///60 segundos de espera

            ///Conectar al Servidor 
            channel.connect();
            transInitATC(channel);
            channel.disconnect();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void transInitATC(ISOChannel channel) throws ISOException, IOException, InterruptedException {
    }
}
